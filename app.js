var express =require('express');
var app = express();
var http = require('http');
var path =require('path');
var server = http.createServer(app);
var socket = require('socket.io');
var io = socket(server);
var mysql = require('mysql');
var bodyParser = require('body-parser');
var request = require('request');				//for external api request
const bcrypt = require('bcrypt');				//for encrypting passwords
var format=require('timestamp-format');
var math = require('mathjs');
var moment = require('moment');
//var env = require('dotenv').config();	                        //for adding timestamp
require('events').EventEmitter.defaultMaxListeners = 100;

var count =1;

/* Database connection */
var mysql = require('mysql');

var connection = mysql.createConnection({
	// host : '34.205.247.1',
	// user : 'forge',
	// password : '2Smk1AdFBYiqHiXTNKc5',
	// database : 'nearme'
	host : 'pack-and-send.c767prcnty97.us-west-2.rds.amazonaws.com',
	user : 'root',
	password : 'pack#123',
	database : 'pack_and_send'
	//host:process.env.DB_HOST,
	//user : process.env.DB_USER,
	//password : process.env.DB_PASSWORD,
	//database : process.env.DB_PASS
});

try{
	connection.connect();
	console.log('Database connection successfull...');
}catch(e){
	console.log('Database connection failed '+ e );
}


/*Configure app */
// app.set('view engine', 'ejs');
// app.set('views',path.join(__dirname,'views'));


/* use middleware */ 
// app.use('/css',express.static(path.join(__dirname,'/css')));
// app.use('/images',express.static(path.join(__dirname,'/images')));
// app.use('/includes',express.static(path.join(__dirname,'/includes')));
// app.use('/js',express.static(path.join(__dirname,'/js')));
// app.use('/',express.static(path.join(__dirname,'')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : true }));
// app.use(expressValidator());
app.set('port',process.env.PORT || 3000);

/* define routes   */
io.on('connection',function(socket){
	console.log('socket connection established.');
	clientSocket = socket;
	var clientID = socket.id;
	console.log('Client :',clientID);
	var job_array= [];

	connection.query('SELECT * from jobs WHERE status != "completed" AND status != "reject"',function(err,result){
		if(!err){
			if(result.affectedRows !=0){
					job_array = result;
					// console.log('job_array : ',job_array);
			}
			else{
				console.log('No jobs are here');
			}
		}
		else{
			console.log('Error in fetching jobs from db');
		}
	});


	socket.on('storeLocation',function(data){
		socket.emit('gpsResponse',{"response" : "success","id" : data.id});
		console.log(data);
		var result = 'lat:' + data.lat + '\n lon' + data.lon + '\n direction' + data.direction ;
		var response = [];
		var status,active,driver_name;
		if(data.lat !== '' && data.lon !== '' && data.driver_id !== '' && data.direction !== ''){
							var lat = data.lat;
							var lon = data.lon;
							var timestamp;
							var job_id,status,deviceId;
							var job_type= data.job_type;
							var driver_id = data.driver_id;
							var deviceId = data.device_id;
							var driver_name =data.driver_name;
							var direction = data.direction;
							var speed = ((18.0/5.0) * data.speed).toFixed(2);
							console.log('type of speed: ',typeof(speed)); 
							if(!data.timestamp || typeof(data.timestamp)=== 'string' ){
								timestamp = 0;
								// console.log("timestamp set to 0 ");
							}
							else{
								timestamp = data.timestamp;
								// console.log("timestamp set to",data.timestamp);
							}

							if(data.job_id == 0){
								job_id = null;
								status = "Idle";
								active = "false";

							}
							else
							{
								job_id = data.job_id;
								status = "In Job";
								active = "true";
							}
							console.log('is Active :' , active);
							// console.log("job_id : " +job_id);


							//geofence breach checking
							if(job_id != null && job_id != 0){
								var job = job_array.find(o => o.ref_id == job_id);
								// console.log("job:",job);
								var earthRadius = 6371000;
						        // convert from degrees to radians
						        var latFrom = (lat/180) * math.PI;
						        var lonFrom = (lon/180) * math.PI;
						        var latTo = (job.end_lat) ? (job.end_lat/180)* math.PI : 0;
						        var lonTo = (job.end_lon) ? (job.end_lon/180)* math.PI : 0;

						        var latDelta = latTo - latFrom;
						        var lonDelta = lonTo - lonFrom;

						        var angle = 2 * math.asin(math.sqrt(math.pow(math.sin(latDelta / 2), 2) + 
								math.cos(latFrom) * math.cos(latTo) * math.pow(math.sin(lonDelta / 2), 2)));
						        var computedDistance = angle * earthRadius;
						        console.log('computedDistance :',computedDistance);
						        console.log('Geo radius:',job.geo_radius);
						        console.log('Notification enabled:',job.notification_enabled);

						        if(job.notification_enabled == 1 && job.geo_radius >= computedDistance){
						        	console.log('Breached');
						        	job.notification_enabled = 0;
						        	requestingData = {
										  	"status" : "breached",
											"signOffDate" : moment().format('DD-MM-YYYY HH:mm:ss'),
											"driverRegID" : job.driver_id,
											"deviceId" : deviceId,
											"jobId" : job.jobNo,
											"latitude" : lat,
											"longitude" : lon
									}
									console.log('requestingData :',requestingData);
						        	//request mavEvent trigger
						         	request({
									    method: 'POST',
									    uri: 'http://108.161.129.6:8180/workflow/rest/api/v1/EventMapService/saveMapEvent',
									    // alternatively pass an object containing additional options
									    json: {
										  	"status" : "breached",
											"signOffDate" : moment().format('YYYY-MM-DD HH:mm:ss'),
											"driverRegID" : job.driver_id,
											"deviceId" : deviceId,
											"jobId" : job.jobNo,
											"latitude" : lat,
											"longitude" : lon
									}
									  },
									  function (error, response, body) {
									    if (error) {
									      return console.error('upload failed:', error);
									    }
									    console.log('Upload successful!  Server responded with:', body);
									    console.log('statusCode:', response && response.statusCode);
									    connection.query('UPDATE jobs SET notification_enabled = 0 WHERE ref_id = ?',[job_id],function(err,result){
						        				if(!err){
													if(result.affectedRows !=0){

														console.log('Job breached notified in the db by setting notification _enabled value as 1');
													}

													else{
														console.log('No fields to update in job breached trigger');
													}
												}
												else{
													console.log('error:',err);
												}
						        			});
									  })


						        }

							}


							if(speed == '0.0'){
								connection.query('SELECT speed FROM trackings WHERE driver_id = ? ORDER BY id LIMIT 5',[driver_id],function(err,result){
									if(!err){
										if(result.affectedRows !=0){	
											console.log('result : ',result);
											var total = 0.0;
											for(i=0;i<result.length;i++){
												total += parseFloat(result[i].speed);
											}
											speed = total/5;
											console.log('speed :',speed);
										}
										else{
											res.status(400);
										}
									}
									else{
										res.status(400).send({"status" : "error","msg" : err.sqlMessage});
									}
								});
							}
							// connection.query('SELECT name,deviceId FROM drivers WHERE driver_id = ?',[driver_id],function(err,result){

							// 	if(!err){
							// 		if(result.affectedRows !=0){
													
							// 			driver_name = result[0].name;
							// 			deviceId = result[0].deviceId;
							// 			console.log('driver_name : ', driver_name,'\n deviceId :',deviceId);
							// 		}
							// 	}

							// });
							console.log(lat,lon,timestamp,driver_id,job_id,direction);
							connection.query('UPDATE drivers SET isActive = ? WHERE driver_id = ?',[active,driver_id],function(err,result){

											if(!err){
												if(result.affectedRows !=0){
													if(active == true){
														console.log("driver status is true");
													}

													io.sockets.emit('statusUpdate',{driver_name : driver_name , driver_id : driver_id,status : status});
													response.push({'status' : 'ok'});
												}

												else{
													respone.push({"status" : "error","message" : "Driver Id not found "});
												}
											}

											else{
												response.push({"status" : "error" , "message " : err.sqlMessage + "update isActive"});
											}

							});
							connection.query('INSERT INTO trackings (lat,lon,timestamp,driver_id,job_id,direction,speed) VALUES (?,?,?,?,?,?,?)',
											[lat,lon,timestamp,driver_id,job_id,direction,speed],
											function(err,result){

													if(!err){
														// console.log("Inside DB query...");
														if(result.affectedRows != 0){
															console.log('driver_name is :',driver_name);

															//send data to NME 
															var nsp = io.of('/NME');
															console.log(nsp.name);
															nsp.on('connection', function(socket){
															  // console.log(count + ' connection established to ' + company_code + ' namespace.');
															}).emit('storeLocation',{lat : lat,lon : lon,driver_id :driver_id,job_id : job_id, direction : direction , status : status , driver_name : driver_name , speed : speed , job_type : job_type , device_id : deviceId });

															// io.sockets.emit('storeLocation',{lat : lat,lon : lon,driver_id :driver_id,job_id : job_id, direction : direction , driver_name : driver_name });
															
															response.push({'status' : 'ok'});
															// console.log(res);

														}

														else{
															response.push({'status' : 'ok','Msg' : 'No Result found'});
														}
														// res.setHeader('Content-Type' , 'application/json');
														// res.status(200).send(JSON.stringify(response));

													}
													else{
														// console.log("Inside else of DB query...",err);
														var error = err;
														console.log("error is :",err);
														response.push({'status' : 'error','Msg' : err.sqlMessage + "insert into tracking"});
														// console.log(JSON.stringify(response));
													}
											});
			}
	});
}); 



/* Set Default Route */
app.get('/',function(req,res){
	// res.sendFile(__dirname + '/test.html');

});


/*Geofence breach API */
app.post('/geofenceBreach',function(req,res){
	console.log(req.body);
	var response = [];
	res.setHeader('Content-Type', 'application/json');
	//Allow access control to all domains
	res.header("Access-Control-Allow-Origin", "*");

	//Check if the api requesting fields are null 
	if(req.body.shopName !== '' && req.body.shopId !== '' && req.body.driverId !=='' && req.body.companyCode !==''){
		var shop_name = req.body.shopName;
		var shop_id = parseInt(req.body.shopId);
		var driver_id = req.body.driverId;
		var company_code = req.body.companyCode;
		var status = 'delivered';
		var time = format('yyyy-MM-dd hh:mm:ss',new Date().getTime());
		console.log(time);
		//Convert the server time to a specific format
		var arrivalTime = format('hh:mm:ss',time);

		connection.query('SELECT status FROM shops WHERE id=?',[shop_id],function(err,result){
			if(!err){
				if(result.affectedRows !=0){
					var delivery_status = result[0].status.toLowerCase();
					
					console.log('delivery_status',delivery_status);
					if(delivery_status == 'pending'){

						//send data to the corresponding Agent
						if(company_code != 'NME'){
							var nsp1 = io.of('/'+ company_code);
							console.log(nsp1.name);
							nsp1.on('connection', function(socket){
							  console.log(count + ' connection established to ' + company_code + ' namespace.');
							}).emit('geofenceBreach',{'shopName' : shop_name,'shopId' : shop_id,'arrivalTime' : arrivalTime });
						}

						//send data to NME 
						var nsp2 = io.of('/NME');
						console.log(nsp2.name);
						nsp2.on('connection', function(socket){
						  console.log(count + ' connection established to ' + company_code + ' namespace.');
						}).emit('geofenceBreach',{'shopName' : shop_name,'shopId' : shop_id,'arrivalTime' : arrivalTime });

						// io.sockets.emit('geofenceBreach',{'shopName' : shop_name,'shopId' : shop_id,'arrivalTime' : arrivalTime });
						
						//Send the data to live-status page
						connection.query('UPDATE shops SET status= ? WHERE id= ?',[status,shop_id],function(err,result){
							if(!err){
								if(result.affectedRows != 0){
									// response.push({"status" : "ok"});
									res.status(200);
								}
								else{
									res.status(400);
								}
							}
							else{
								console.log("update status failed.. ", err);
								res.status(400);
							}
						});


						connection.query('INSERT INTO delivery_list(shop_name,shop_id,driver_id,agent_name) VALUES (?,?,?,?)',
							[shop_name,shop_id,driver_id,company_code],
							function(err,result){
							if(!err){
								if(result.affectedRows != 0){
									// response.push({"status" : "ok"});
									console.log("inserting into delivery_status success..");
									res.status(200);
								}
								else{
									console.log("insert into delivery_list ok..but no data.." + result)
									res.status(400);
								}
							}
							else{
								console.log("inserting into delivery_status failed..  " + err);
								res.status(400);
							}
						});
					}
					else{

						//send data to NME 
						var nsp2 = io.of('/NME');
						console.log(nsp2.name);
						nsp2.on('connection', function(socket){
						  console.log(count + ' connection established to ' + company_code + ' namespace.');
						}).emit('geofenceBreach',{'shopName' : shop_name,'shopId' : shop_id,'arrivalTime' : arrivalTime });


						// io.sockets.emit('geofenceBreach',{'shopName' : shop_name,'shopId' : shop_id,'arrivalTime' : arrivalTime });
					}
					res.status(200).send({"status" : "ok"});
				}
				else{
					res.status(400).send({"status" : "error","msg" : "No result found."});
				}
			}
			else{
				res.status(400).send({"status" : "error" , "error" : err.sqlMessage});
			}
		});

	}

	else {
		response.push({'status' : 'error', 'msg' : 'Please fill required details'});
    	res.status(400).send(JSON.stringify(response));
	}

});

/*Geofence complete API */
app.post('/geofenceComplete',function(req,res){
	console.log(req.body);
	var response = [];
	res.setHeader('Content-Type', 'application/json');
	res.header("Access-Control-Allow-Origin", "*");
	if(req.body.shopName !== '' && req.body.shopId !== '' && req.body.driverId !=='' && req.body.companyCode !==''){
		var shop_name = req.body.shopName;
		var shop_id = parseInt(req.body.shopId);
		var driver_id = req.body.driverId;
		var company_code = req.body.companyCode;
		var status = 'completed';
		var time = format('yyyy-MM-dd hh:mm:ss',new Date().getTime());
		console.log(time);
		var completedTime = format('hh:mm:ss',time);

		//send data to the corresponding Agent
		if(company_code != 'NME'){
			var nsp1 = io.of('/'+ company_code);
			console.log(nsp1.name);
			nsp1.on('connection', function(socket){
			  console.log(count + ' connection established to ' + company_code + ' namespace.');
			}).emit('geofenceComplete',{'shopName' : shop_name,'shopId' : shop_id });
		}
		//send data to NME 
		var nsp2 = io.of('/NME');
		console.log(nsp2.name);
		nsp2.on('connection', function(socket){
		  console.log(count + ' connection established to ' + company_code + ' namespace.');
		}).emit('geofenceComplete',{'shopName' : shop_name,'shopId' : shop_id });


		// io.sockets.emit('geofenceComplete',{'shopName' : shop_name,'shopId' : shop_id });

		//Send the data to live-status page
		connection.query('UPDATE shops SET status= ? WHERE id= ?',[status,shop_id],function(err,result){
			if(!err){
				if(result.affectedRows != 0){
					// response.push({"status" : "ok"});
					res.status(200).send({"status" : "ok"});
				}
				else{
					// response.push({"status" : "ok","msg" : "No result found."});
					res.status(400).send({"status" : "error","msg" : "No result found."});
				}
			}
			else{
				res.status(400).send({"status" : "error" , "error" : err.sqlMessage});
			}
		});

		connection.query('UPDATE delivery_list SET date_completed= ? WHERE shop_id= ?',[completedTime,shop_id],function(err,result){
			if(!err){
				if(result.affectedRows != 0){
					res.status(200);
				}
				else{
					res.status(400);
				}
			}
			else{
				res.status(400);
			}
		});
	}

	else {
		response.push({'status' : 'error', 'msg' : 'Please fill required details'});
    	res.status(400).send(JSON.stringify(response));
	}

});
/* Shop add API */
app.post('/addShop',function(req,res){
	console.log(req.body);
	var response = [];
	res.setHeader('Content-Type','application/json');
	//Allow access control to all domains
	res.header("Access-Control-Allow-Origin", "*");

	if(
		req.body.shopName!== '' && req.body.state!== '' && req.body.companyCode!== '' && req.body.lat !== '' &&
		req.body.lon !== '' && req.body.geoRadius !== '' &&  req.body.status !== '' && req.body.unitNo !== '' &&
		req.body.street !== '' && req.body.postalCode !== '' && req.body.subUrb !== '' && req.body.siteId !== '' 
		 ){

			var shop_name = req.body.shopName;
			var state = req.body.state;
			var company_code = req.body.companyCode;
			var lat = req.body.lat;
			var lon = req.body.lon;
			var geo_radius = req.body.geoRadius;
			var status = req.body.status;
			var unit_no = req.body.unitNo;
			var street = req.body.street;
			var postal_code = req.body.postalCode;
			var sub_urb = req.body.subUrb;
			var site_id = req.body.siteId;

			connection.query('SELECT * FROM shops WHERE lat = ? AND lon = ?',[lat,lon],function(err,result){
				if(!err){
					if(result.length == 0){
						console.log("New shop added");
						connection.query('INSERT INTO shops(shop_name,state,companyCode,lat,lon,geo_radius,status,\
						unitNo,street,postalCode,suburb,siteId) \
						VALUES (?,?,?,?,?,?,?,?,?,?,?,?)',
						[shop_name,state,company_code,lat,lon,geo_radius,status,unit_no,street,postal_code,sub_urb,site_id],
						function(err,result){

							if(!err){

								if(result.affectedRows != 0){
								// response.push({'status' : 'ok'});
									connection.query('SELECT id FROM shops WHERE shop_name=?',[shop_name],function(err,result){

										if(!err){

											if(result.affectedRows != 0){
												var id = result[0].id;
												console.log(id);

												// send data to NME namespace
												var nsp = io.of('/NME');
												console.log(nsp.name);
												nsp.on('connection', function(socket){
												  console.log(count + ' connection established to ' + company_code + ' namespace.');
												}).emit('addShop',{'id' : id,'shop_name' : shop_name,'state' : state ,'companyCode' : company_code,'lat' : lat,'lon' : lon ,'geoRadius' :geo_radius,
														'status' : status,'unitNo' : unit_no,'street' : street,'postalCode' : postal_code,'subUrb' : sub_urb,'siteId' : site_id});
												// io.sockets.emit('addShop',{'id' : id,'shop_name' : shop_name,'state' : state ,'companyCode' : company_code,'lat' : lat,'lon' : lon ,'geoRadius' :geo_radius,
												// 'status' : status,'unitNo' : unit_no,'street' : street,'postalCode' : postal_code,'subUrb' : sub_urb,'siteId' : site_id});
												// console.log(res);
												res.status(200).send({'status' : 'ok'});
											}

											else{
												res.status(400).send({"status" : "ok","Msg" : "No Result found"});
											}
											// res.setHeader('Content-Type' , 'application/json');
											// res.status(200).send(JSON.stringify(response));

										}
										else{
											// res.status(400).send({"status" : "error","Error" : err.sqlMessage});
										}

									});
								}

								else{
										res.status(400).send({"status" : "error","Msg" : "No Result found"});
								}

							}
							else{
								res.status(400).send({"status" : "error","Error" : err.sqlMessage});
							}

						});
					}
					else{
						console.log("Affected rows",result.length);
						console.log("Shop already exist");
						res.status(400).send({"status" : "error","Msg" : "Shop already exist"})
					}
				}
				else{
					res.status(400).send({"status" : "error","Error" : err.sqlMessage});
				}
			});

	}
	else{
    	res.status(400).send({'status' : 'error', 'msg' : 'Please fill required details'});
	}

});

/* shop Delete API */
app.delete('/addShop',function(req,res){
	console.log(req.body);
	res.setHeader('Content-Type','application/json');
	//Allow access control to all domains
	res.header("Access-Control-Allow-Origin", "*");

	if(req.body.id != ''){

		var id = req.body.id;

		connection.query('SELECT companyCode FROM shops WHERE id=?',[id],
		function(err,result){

			if(!err){

				if(result.affectedRows != 0){
					var company_code = result[0].companyCode;
					connection.query('DELETE FROM shops WHERE id=?',[id],function(err,result){

						if(!err){

							if(result.affectedRows != 0){

								// send data to NME namespace
								var nsp = io.of('/NME');
								console.log(nsp.name);
								nsp.on('connection', function(socket){
								  console.log(count + ' connection established to ' + company_code + ' namespace.');
								}).emit('removeShop',{'shop_id' : id,'companyCode' : company_code});
								res.status(200).send({'status' : 'ok'});
								console.log('shop with id ',id,' is removed.');
							}

							else{
								res.status(400).send({"status" : "ok","Msg" : "No Result found"});
							}

						}
						else{
							// res.status(400).send({"status" : "error","Error" : err.sqlMessage});
						}

					});
				}

				else{
						res.status(400).send({"status" : "ok","Msg" : "No Result found"});
				}

			}
			else{
				res.status(400).send({"status" : "error","Error" : err.sqlMessage});
			}

		});

	}
	else{
		res.status(400).send({'status' : 'error', 'msg' : 'Please fill required details'});
	}

});


/* Agent add API */
app.post('/addAgent',function(req,res){
	console.log(req.body);
	var response = [];
	res.setHeader('Content-Type','application/json');
	//Allow access control to all domains
	res.header("Access-Control-Allow-Origin", "*");

	if(req.body.companyName != '' && req.body.companyCode != ''){

			var user_name = req.body.companyCode.toLowerCase();
			var password;
			var company_name = req.body.companyName;
			var company_code = req.body.companyCode.toLowerCase();
			var type = 'agent';
			var customer_id = '';

			password = bcrypt.hashSync(company_code,10);
			console.log("hashed password :",password);
			var request_duration_api= 'http://bread.staging.on.logidots.com/api/createAutoLogin/'+ company_code;
				request(request_duration_api, function (error, response1, body) {
					if(error)
				  		console.log('error:', error); // Print the error if one occurred
				  	else {
				  		console.log('body:', body);
					  	var data = JSON.parse(body);

					  	connection.query('INSERT INTO logins(username,password,type,companyName,companyCode,customerId) \
						VALUES (?,?,?,?,?,?)',
						[user_name,password,type,company_name,company_code,customer_id],
						function(err,result){

							if(!err){

								if(result.affectedRows != 0){
									res.status(200).send({'status' : 'ok','url' : data.url });
									console.log('url : ', data.url);
								}

								else{
									res.status(200).send({"status" : "ok","Msg" : "No Result found"});
								}	

							}
							else{
								res.status(400).send({"status" : "error","Error" : err.sqlMessage});
							}

						});
				  	}
				  	
				});

	}
	else{
		res.status(400).send({'status' : 'error', 'msg' : 'Please fill required details'});
	}

});

// delete an agent API
app.delete('/addAgent',function(req,res){
	console.log(req.body);
	if(req.body.id != ''){
		var id = req.body.id;
		res.setHeader('Content-Type','application/json');
		//Allow access control to all domains
		res.header("Access-Control-Allow-Origin", "*");

		connection.query('DELETE FROM logins WHERE id=?',[id],
		function(err,result){
			if(!err){
				if(result.affectedRows != 0){
					res.status(200).send({'status' : 'ok'});
					console.log("agent with driver ID " + id + "deleted.");
				}

				else{
					res.status(200).send({"status" : "error","Msg" : "No Id found"});
				}
			}
			else{
				res.status(400).send({"status" : "error","Error" : err.sqlMessage});
			}
		});
	}
	else{
		response.push({'status' : 'error', 'msg' : 'Please fill required details'});
	}

});


/*Job Status API */
app.post('/jobStatus',function(req,res){
	console.log(req.body);
	//Allow access control to all domains
	res.header("Access-Control-Allow-Origin", "*");

	if(req.body.driver_id !== '' && req.body.job_id !== '' && req.body.status !==''){
		var driver_id = req.body.driver_id;
		var job_id = req.body.job_id;
		var status = req.body.status.toLowerCase();
		var time = format('yyyy-MM-dd hh:mm:ss',new Date().getTime());
		console.log(time);
		if(status == 'accept'){
			console.log('Job is started');
			status = "In Job";
			connection.query('UPDATE jobs SET status= ?,start_time = ? WHERE ref_id= ?',[status,time,job_id],function(err,result){
				if(!err){
					if(result.affectedRows != 0){
						res.status(200).send({"status" : "ok"});
					}
					else{
						res.status(400).send({"status" : "error","msg" : "No result found."});
					}
				}
				else{
					res.status(400).send({"status" : "error" , "error" : err.sqlMessage});
				}
			});
		}
		else if(status == 'reject'){
			console.log('Job is rejected');
			status = "reject";
			connection.query('UPDATE jobs SET status= ? WHERE ref_id= ?',[status,job_id],function(err,result){
				if(!err){
					if(result.affectedRows != 0){
						res.status(200).send({"status" : "ok"});
					}
					else{
						res.status(400).send({"status" : "error","msg" : "No result found."});
					}
				}
				else{
					res.status(400).send({"status" : "error" , "error" : err.sqlMessage});
				}
			});
		}
		else if(status == 'completed'){
			console.log('Job is ended');
			status = "completed";
			connection.query('UPDATE jobs SET status= ?,end_time = ? WHERE ref_id= ?',[status,time,job_id],function(err,result){
				if(!err){
					if(result.affectedRows != 0){
						res.status(200).send({"status" : "ok"});
					}
					else{
						res.status(400).send({"status" : "error","msg" : "No result found."});
					}
				}
				else{
					res.status(400).send({"status" : "error" , "error" : err.sqlMessage});
				}
			});
		}

	}
	else{
		res.status(400).send({"status" : "error","msg" : "Please fill required details"});
	}
});


/* Job Update API */
app.post('/addJob',function(req,res){
	console.log(req.body);
	res.setHeader('Content-Type','application/json');
	//Allow access control to all domains
	res.header("Access-Control-Allow-Origin", "*");

	if(
		req.body.driver_id!= '' && req.body.ref_id != '' &&  req.body.consignment != '' && req.body.job_type != '' &&
		req.body.jobNo != '' && req.body.status != ''
		 ){

			var driver_id = req.body.driver_id;
			var start_lat = req.body.start_lat;
			var start_lon = req.body.start_lon;
			var end_lat = req.body.end_lat;
			var end_lon = req.body.end_lon;
			var start_time = req.body.start_time;
			var end_time = req.body.end_time;
			var ref_id = req.body.ref_id;
			var consignment = req.body.consignment;
			var bookingReference = req.body.bookingReference;
			var job_type = req.body.job_type;
			var pickup_address = req.body.pickup_address;
			var delivery_address = req.body.delivery_address;
			var jobNo = req.body.jobNo;
			var status = req.body.status;
			var pickup_suburb = req.body.pickup_suburb;
			var delivery_suburb = req.body.delivery_suburb;
			var pickup_state = req.body.pickup_state;
			var delivery_state = req.body.delivery_state;
			var breach_type = (req.body.breach_type)? req.body.breach_type : null ;
			var time_to_breached = req.body.time_to_breached;
			var notification_enabled;
			var duration = 0;
			var distance = 0;
			var avg_speed = 40;
			var geo_radius;

			if(breach_type == 'T'){
				console.log('Enterd as time..');
				geo_radius = avg_speed * (5/18) * time_to_breached * 60;
				console.log('Geo_radius:',geo_radius);
				notification_enabled = true;
				
			}
			else if(breach_type == 'D'){
				console.log('Enterd as distance..');
				geo_radius = time_to_breached;
				notification_enabled = true;
			}
			else if(breach_type == '' || breach_type == null){
				console.log('No type entered..');
				notification_enabled = false;
			}
			else{
				console.log('Wrong type');
				notification_enabled = false;
			}


			if(job_type != 'WO' &&  pickup_address != '' && delivery_address != ''){
				//Duration calculation for the job
				var request_duration_api= 'https://maps.googleapis.com/maps/api/directions/json?origin='+ encodeURI(pickup_address)+ '&destination=' + encodeURI(delivery_address) + '&key=AIzaSyD1K5TIiR1RxDyPogx9bD3CnlmmB6hxQ3U';
				request(request_duration_api, function (error, response1, body) {
					if(error)
				  		console.log('error:', error); // Print the error if one occurred

					// console.log('body:', body); Print the HTML for the Google homepage.
				  	var data = JSON.parse(body);
				  	//calculate the time duration b/w pickup and delivery addresses.
				  	if(data.routes[0]){
				  		duration = data.routes[0].legs[0].duration.text;
					  	console.log("duration:",duration);

					  	//calculate the distance b/w pickup and delivery addresses.
					  	distance = data.routes[0].legs[0].distance.text.slice(0,-2);
				  	} 
				  	else{
				  		console.log("Give proper and perfect address");
				  		duration = 0;
				  		distance = 0;
				  	}

				  	console.log("distance :",distance);
				  	connection.query('INSERT INTO jobs(driver_id,start_lat,start_lon,end_lat,end_lon,ref_id,consignment,bookingReference,job_type,\
						  pickup_address,delivery_address,jobNo,status,pickup_suburb,delivery_suburb,pickup_state,delivery_state,duration,distance,breach_type,geo_radius,notification_enabled) \
						  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
						  [driver_id,start_lat,start_lon,end_lat,end_lon,ref_id,consignment,bookingReference,job_type,pickup_address,
						  delivery_address,jobNo,status,pickup_suburb,delivery_suburb,pickup_state,delivery_state,duration,distance,breach_type,geo_radius,notification_enabled],
						  function(err,result){

								if(!err){

										if(result.affectedRows != 0){
											// res.status(200).send({'status':'ok'});
											// console.log(res);
											connection.query('SELECT start_time,end_time FROM jobs WHERE ref_id = ? LIMIT 1',[ref_id],
												function(err,result){

														if(!err){

																if(result.affectedRows != 0){
																	// console.log("select result :" + result[0]);
																	var start_time = result[0].start_time;
																	var end_time = result[0].end_time;
																	// send data to NME namespace
																	var nsp = io.of('/NME');
																	console.log(nsp.name);
																	nsp.on('connection', function(socket){
																	  // console.log(count + ' connection established to ' + company_code + ' namespace.');
																	}).emit('addJob',{driver_id : driver_id , jobNo : jobNo ,ref_id : ref_id,status : status, start_time :start_time,end_time : end_time});
																	res.status(200).send({'status' : 'ok'});
																}

																else{
																	res.status(200).send({"status" : "error","msg" : 'No data found'});
																}

														}
														else{
																res.status(400).send({"status" : "error","msg" : err.sqlMessage});
														}

												});
										}

										else{
											res.status(400).send({"status" : "error","Msg" : "No Result found"});
										}
										
										// res.status(200).send(JSON.stringify(response));

								}
								else{
										res.status(400).send({"status" : "error","error" : err.sqlMessage});
								}

					});

				});
			}
	}
	else{
    	res.status(400).send({'status' : 'error', 'msg' : 'Please fill required details'});
	}

});
/*Job delete API */

app.delete('/addJob',function(req,res){
	console.log(req.body);
	res.setHeader('Content-Type','application/json');
	//Allow access control to all domains
	res.header("Access-Control-Allow-Origin", "*");

	if(req.body.job_id != ''){
		var job_id = req.body.job_id;
		connection.query('DELETE FROM jobs WHERE ref_id=?',[job_id],function(err,result){
			if(!err){
				if(result.affectedRows !=0){
					res.status(200).send({'status' : 'ok'});
					console.log("Job with job ID " + job_id + " deleted.");
				}
				else{
					res.status(400).send({'status' : 'error' , 'msg' : 'Job Id not found'});
				}
			}
			else{
				res.status(400).send({'status' : 'error' , 'msg' : err.sqlMessage});
			}
		});
	}
	else{
		res.status(400).send({'status' : 'error' , 'msg' : 'Please fill required details'});
	}
});


/* add Driver API */
app.post('/addDriver',function(req,res){
	console.log(req.body);
	res.setHeader('Content-Type', 'application/json');
	//Allow access control to all domains
	res.header("Access-Control-Allow-Origin", "*");
	
	var response = [];
	if(
		req.body.name!== '' && req.body.driver_id!== '' && req.body.gcmId !== ' ' && 
		req.body.registrationId!== '' && req.body.registeredStatus != '' && req.body.companyName !== '' && req.body.country !== '' &&  
		req.body.tz_id !== '' &&  req.body.isActive !== '' && req.body.deviceName !== '' && req.body.email !== '' && 
		req.body.longitude !== '' && req.body.latitude !== '' &&req.body.deviceId !== '' && req.body.mobileNo !== '' && 
		req.body.status !== ''
		){
			var name = req.body.name;
			var driver_id = req.body.driver_id;
			var gcmId = req.body.gcmId;
			var registrationId = req.body.registrationId;
			var registeredStatus = req.body.registeredStatus;
			var companyName = req.body.companyName;
			var country = req.body.country;
			var tz_id = req.body.tz_id;
			var isActive = req.body.isActive;
			var deviceName = req.body.deviceName;
			var email = req.body.email;
			var longitude = req.body.longitude;
			var latitude = req.body.latitude;
			var deviceId = req.body.deviceId;
			var mobileNo = req.body.mobileNo;
			var status = req.body.status;
			// var companyCode = req.body.companyCode;

			// If the driver is already registered,then update the driver
			connection.query('SELECT driver_id FROM drivers where driver_id = ?',[driver_id],function(err,result){
				if(!err){
					console.log(result[0]);
					if(result.length == 0){
						connection.query('INSERT INTO drivers(name,driver_id,gcmId,registrationId,registeredStatus,companyName,country,tz_id,isActive,\
							  deviceName,email,longitude,latitude,deviceId,mobileNo,status) \
							  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
							  [name,driver_id,gcmId,registrationId,registeredStatus,companyName,country,tz_id,isActive,deviceName,email,
							  longitude,latitude,deviceId,mobileNo,status],
							  function(err,result){

									if(!err){

											if(result.affectedRows != 0){
												res.status(200).send({'status' : 'ok'});
												// console.log(res);

												// send data to NME namespace
												// var nsp = io.of('/NME');
												// console.log(nsp.name);
												// nsp.on('connection', function(socket){
												//   // console.log(count + ' connection established to ' + company_code + ' namespace.');
												// }).emit('addDriver',{driver_id : driver_id,name : name ,lat :latitude , lon : longitude ,direction : 0,companyName : companyName});

												// io.sockets.emit('addDriver',{driver_id : driver_id,driver_name : name ,lat :latitude , lon : longitude ,direction : 0,companyName : companyName});
											}

											else{
												res.status(200).send({'status':'ok','msg' : 'No Result found'});
											}

									}
									else{
											res.status(400).send({'error' : err.sqlMessage});
									}

							});
					}
					else{
						connection.query('UPDATE drivers SET gcmId = ? WHERE driver_id = ?',[gcmId,driver_id],function(err,result){

									if(!err){

											if(result.affectedRows != 0){
												res.status(200).send({'status':'ok'});				
											}

											else{
												res.status(200).send({'status':'ok','msg' : 'No Result found'});
											}

									}
									else{
											res.status(400).send({'error' : err.sqlMessage});
									}

							});
					}
				}
				else{
					res.status(400).send({'status' : 'error' , 'msg' : err.sqlMessage});
				}
			});
	}
	else{
		res.status(400).send({'status' : 'error', 'msg' : 'Please fill required details'});
	}
});

/* Login API */
app.post('/login',function(req,res){
	var response =[];
	var status = req.body.status;
	var driver_id = req.body.driver_id;
	res.setHeader('Content-Type','application/json');
	//Allow access control to all domains
	res.header("Access-Control-Allow-Origin", "*");

	connection.query('SELECT companyName FROM drivers WHERE driver_id = ?',[driver_id],function(err,result){
		if(!err){
			console.log(result[0]);
			if(result.length != 0){
				var company_name = result[0].companyName.toLowerCase();
				if(company_name){
					connection.query('UPDATE drivers SET status = ? WHERE driver_id = ?',[status,driver_id],function(err,result){

						if(!err){

								if(result.affectedRows != 0){
									if(status == 0){

										// send data to NME namespace
										var nsp = io.of('/NME');
										console.log(nsp.name);
										nsp.on('connection', function(socket){
										  // console.log(count + ' connection established to ' + company_code + ' namespace.');
										}).emit('logout',{driver_id : driver_id });

										// io.sockets.emit('logout',{driver_id : driver_id });
										res.status(200).send({"status" : "ok"});
									}
									else{
										connection.query('SELECT name,companyName FROM drivers WHERE driver_id = ?',[driver_id],
										function(err,result){

												if(!err){

														if(result.length != 0){
															console.log(result[0]);
															console.log("selected result :" + result[0].name);
															var name = result[0].name;
															var companyName = result[0].companyName;

															// send data to NME namespace
															var nsp = io.of('/NME');
															console.log(nsp.name);
															nsp.on('connection', function(socket){
															  // console.log(count + ' connection established to ' + company_code + ' namespace.');
															}).emit('login',{driver_id : driver_id,name : name,companyName : companyName });

															// io.sockets.emit('login',{driver_id : driver_id,name : name,companyName : companyName });
															console.log(driver_id,name);
															res.status(200).send({"status" : "ok"});
														}

														else{
															res.status(400).send({"status" : "error","message" : "Driver not found "});
														}

												}
												else{
														res.status(400).send({"status" : "error","message" :err.sqlMessage });
												}

										});
									}
								}

								else{
									res.status(400).send({"status" : "error","message" : "no data found"});
								}

						}
						else{
								res.status(400).send({"status" : "error","message" : err.sqlMessage});
						}
					});
				}
				else{
					console.log('Sorry...This driver is not a part of NME');
					res.status(400).send({'status' : 'error' , 'message' : 'Sorry...This driver is not a part of NME'});
				}
			}
			else{
				res.status(400).send({'status' : 'error' , 'message' : 'No driver found with the ID ' + driver_id});
			}
		}
		else{
			res.status(400).send({'status' : 'error' , 'message' : err.sqlMessage});
		}
	});
	
});

console.log("Running in :"  + process.env.NODE_ENV);

/*Create Server */
server.listen(3000,'127.0.0.1',function(){
	console.log('server running at port 3000');
});


// http.createServer(function(req,res){
// 	res.writeHead(200,{'Content-Type' : 'text/plain' });
// 	res.end('Hello world');
// }).listen(3000,'127.0.0.1');
// console.log('Server running at http://127.0.0.1/3000/');
